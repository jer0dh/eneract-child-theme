<?php
/**
 * EnerACT Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package EnerACT
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ENERACT_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'eneract-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ENERACT_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );


add_shortcode( 'category_search', 'ea_category_search' );
/**
 * Shortcode to add modified Search Form
 */
function ea_category_search() {
	add_filter( 'get_search_form', 'ea_add_categories_and_divs', 99 );
	get_search_form( array( 'aria_label' => 'Search Posts By Category' ) );
}

/**
 * A filter to alter the search form with the addition of a categories dropdown and
 * and inserts hidden values like post_type.
 *
 * @param string $form  Form markup.
 * @param string $search_text  Search text.
 * @param string $button_text  Button text.
 * @return string
 */
function ea_add_categories_and_divs( $form ) {
	$count = 1;
	// Add divs around existing search and submit inputs.
	$search_term = get_query_var( 's' );
	if ( '' !== $search_term ) {
		$form = str_replace( 'value="" name="s"', 'value="' . sanitize_text_field( $search_term ) . '" name="s"', $form, $count );

	}
	$form = str_replace( 'class="search-form"', 'class="search-form search-form-categories"', $form, $count );
	$form = str_replace( '<label', '<div><label', $form, $count );
	$form = str_replace( '</form>', '<input type="hidden" name="post_type" value="post" class="sr-only"></div></form>', $form, $count );
	// $form = str_replace( 'placeholder', 'placeholder-bad', $form, $count );
	
	$selected_cat = get_query_var( 'cat' );
	$dropdown     = '<div><label class="search-form-label sr-only" for="cat">' . __( 'Choose a Category', 'mr-theme' ) . '</label>';
	$dropdown    .= '<div class="dropdown">' . wp_dropdown_categories(
		array(
			'hide_if_empty'   => true,
			'echo'            => 0,
			'show_option_all' => __( 'Choose a Category', 'mr-theme' ),
			'selected'        => $selected_cat,
			'orderby'         => 'name',
		)
	);

	$dropdown .= '</div></div>';

	// add onchange attribute.
	$dropdown = str_replace( '<select', '<select onchange="this.form.submit()" ', $dropdown );
	return str_replace( '<div>', $dropdown . '<div>', $form, $count );
}
